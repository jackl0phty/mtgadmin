# mtgadmin
[![Latest Release](https://gitlab.com/jackl0phty/mtgadmin/-/badges/release.svg)](https://gitlab.com/jackl0phty/mtgadmin/-/releases)
[![pipeline status](https://gitlab.com/jackl0phty/mtgadmin/badges/main/pipeline.svg)](https://gitlab.com/jackl0phty/mtgadmin/-/commits/main)
[![coverage report](https://gitlab.com/jackl0phty/mtgadmin/badges/main/coverage.svg)](https://gitlab.com/jackl0phty/mtgadmin/-/commits/main)


A Rust Daemon That Listens for File Changes for MTG Arena Tool &amp; Automatically Submits Logs

## How This Project Was Built

Install Rust as a non-priv user & reload Cargo ENV.
```bash
$ cd /tmp
$ curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs -o rust.sh
$ vim rust.sh
$ chmod u+x rust.sh
$ ./rust.sh
$ source $HOME/.cargo/env
```

Confirm Rust Version & That You-re up-to-date.
```bash
$ rustup self update
info: checking for self-update
  rustup unchanged - 1.26.0
```

Follow Rust Nightly Build.
```bash
$ rustup default nightly
```

### Initial Project Setup, build & run mtgadmin
```bash
$ cargo new mtgadmin --bin
     Created binary (application) `mtgadmin` package
```
```bash
$ cd mtgadmin/src
$ cargo build
```
```bash
rustc main.rs
```

#### Testing
Check crate's semver version
```bash
$ cargo install cargo-semver-checks --locked
$ cargo semver-checks check-release
```
Run cargo-checkmate
```bash
cargo install cargo-checkmate
cargo checkmate

```

#### Hard Requirements for Using mtgadmin
In order to make FULL use of mtgadmin the following are/will be HARD requirements!

- You MUST have a free account at [mtgatool.com](https://app.mtgatool.com/auth).
- You MUST have the mtgatool Linux client [installed](https://github.com/mtgatool/mtgatool-desktop/blob/dev/README.md#install-on-linux).
- You MUST create a free 17lands [account](https://www.17lands.com/register).
- You MUST have 17lands Linux client [installed](https://www.17lands.com/getting_started).


#### mtgadmin Usage
Upload game data to the 17lands API.
```
sudo mtgadmin -m '/mnt/disk3/lutris/mtg_arena/drive_c/Program Files/Wizards of the Coast/MTGA/MTGA_Data/Logs/Logs' -a '17lands'
[1/4] 🚚  Getting MTG Arena Tool log directory to monitor from user...
Monitoring MTG Arena Tool log directory /mnt/disk3/lutris/mtg_arena/drive_c/Program Files/Wizards of the Coast/MTGA/MTGA_Data/Logs/Logs
[2/4] 📃  Getting API endpoint from user...!
API endpoint to upload game data to: 17lands
17lands is a supported API endpoint!
[3/4] ✨ Uploading MTGA game data to 17lands...!
Uploading MTGA game data to API endpoint 17lands
"/mnt/disk3/lutris/mtg_arena/drive_c/Program Files/Wizards of the Coast/MTGA/MTGA_Data/Logs/Logs/UTC_Log - 03-01-2023 22.33.07.log" appears to be an MTGA game log file
Debug info: Ok(Output { status: ExitStatus(unix_wait_status(0)), stdout: "", stderr: "20230530 154338,INFO,Saving logs to /home/vader/.seventeenlands/seventeenlands.log\n20230530 154339,INFO,Got minimum client version response: {\"is_supported\": true, \"latest_version\": \"0.1.34\", \"min_version\": \"0.1.37\", \"upgrade_instructions\": \"17Lands update required! The minimum supported version for the client is 0.1.37.  Please update with one of the following commands in the terminal, depending on your installation method:\\nbrew update && brew upgrade seventeenlands\\npip3 install --user --upgrade seventeenlands\"}\n20230530 154339,INFO,Minimum supported version: [0, 1, 37]; this version: [0, 1, 39]\n20230530 154339,INFO,Using token 1f4a...7fa0\n20230530 154339,WARNING,Found no files to parse. Try to find Arena's Player.log file and pass it as an argument with -l\n20230530 154339,INFO,Exiting\n" })
Uploading MTGA game data to 17lands...
"/mnt/disk3/lutris/mtg_arena/drive_c/Program Files/Wizards of the Coast/MTGA/MTGA_Data/Logs/Logs/UTC_Log - 03-10-2023 22.03.06.log" appears to be an MTGA game log file
Debug info: Ok(Output { status: ExitStatus(unix_wait_status(0)), stdout: "", stderr: "20230530 154339,INFO,Saving logs to /home/vader/.seventeenlands/seventeenlands.log\n20230530 154339,INFO,Got minimum client version response: {\"is_supported\": true, \"latest_version\": \"0.1.34\", \"min_version\": \"0.1.37\", \"upgrade_instructions\": \"17Lands update required! The minimum supported version for the client is 0.1.37.  Please update with one of the following commands in the terminal, depending on your installation method:\\nbrew update && brew upgrade seventeenlands\\npip3 install --user --upgrade seventeenlands\"}\n20230530 154339,INFO,Minimum supported version: [0, 1, 37]; this version: [0, 1, 39]\n20230530 154339,INFO,Using token 1f4a...7fa0\n20230530 154339,WARNING,Found no files to parse. Try to find Arena's Player.log file and pass it as an argument with -l\n20230530 154339,INFO,Exiting\n" })
Uploading MTGA game data to 17lands...
[4/4] 🚚  Demonizing mtgadmin...!
```

#### Future of mtgadmin
- Polish support for 17lands API.
- Add support for uploading to MTGA Tool's [aggregator](https://app.mtgatool.com/aggregator).
- Add support for [magicthegathering](https://docs.magicthegathering.io/) API.

So stay tuned. I've got big plans!
