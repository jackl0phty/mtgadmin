// Import libraries
use clap::Parser;

// Set about for cli
#[derive(Parser, Debug)]
#[structopt(
    name = "mtgadmin",
    about = "A Rust Daemon That Listens for MTGA game log changes & uploads to various APIs. Supported APIs: '17lands'."
)]

#[clap(author, about, long_about = None)]
#[clap(version = "1.0.5")]

// CLI arguments
struct Args {
    #[arg(short, long)]
    mtg_arena_tool_log_dir: String, // Directory with MTG Arena logs
    #[arg(short, long)]
    api_endpoint: String, // API endpoint to upload data to
}

/////  BEGIN: fn main()  /////
fn main() {

    use std::thread;
    use std::time::Duration;

    use console::{style, Emoji};
    use indicatif::{HumanDuration, MultiProgress, ProgressBar, ProgressStyle};
    //use rand::seq::SliceRandom;
    //use rand::Rng;

    // Declare statics used by crate indicatif.
    static TRUCK: Emoji<'_, '_> = Emoji("🚚  ", "");
    static SPARKLE: Emoji<'_, '_> = Emoji("✨ ", ":-)");
    static PAPER: Emoji<'_, '_> = Emoji("📃  ", "");

    // Might need these in the future.
    //static LOOKING_GLASS: Emoji<'_, '_> = Emoji("🔍  ", "");
    //static CLIP: Emoji<'_, '_> = Emoji("🔗  ", "");

    // Initialize Args construct.
    let args = Args::parse();

    // Uncomment below to debug variables
    //println!("var -> mtg_arena_tool_log_dir: {:#?}", args.mtg_arena_tool_log_dir);
    //println!("var -> api_endpoint: {:#?}", args.api_endpoint);

    /////  BEGIN: fn get_log_dir()  /////
    println!(
        "{} {}Getting MTG Arena Tool log directory to monitor from user...",
        style("[1/4]").bold().dim(),
        TRUCK
    );

    use mtgadmin::get_log_dir;
    get_log_dir(&args.mtg_arena_tool_log_dir);

    let deps = 150;
    let pb = ProgressBar::new(deps);
    for _ in 0..deps {
        pb.inc(1);
        thread::sleep(Duration::from_millis(3));
    }
    pb.finish_and_clear();

    /////  BEGIN: fn get_api_endpoint()  /////
    println!(
        "{} {}Getting API endpoint from user...!",
        style("[2/4]").bold().dim(),
        PAPER
    );

    use mtgadmin::get_api_endpoint;
    get_api_endpoint(&args.api_endpoint);

    let deps = 150;
    let pb = ProgressBar::new(deps);
    for _ in 0..deps {
        pb.inc(1);
        thread::sleep(Duration::from_millis(3));
    }
    pb.finish_and_clear();

    /////  BEGIN: fn upload_game_data_to_17lands()  /////
    println!(
        "{} {}Uploading MTGA game data to 17lands...!",
        style("[3/4]").bold().dim(),
        SPARKLE
    );

    use mtgadmin::upload_game_data_to_17lands;
    upload_game_data_to_17lands(&args.mtg_arena_tool_log_dir, &args.api_endpoint);

    let deps = 150;
    let pb = ProgressBar::new(deps);
    for _ in 0..deps {
        pb.inc(1);
        thread::sleep(Duration::from_millis(3));
    }
    pb.finish_and_clear();

    /////  BEGIN: fn demonize()  /////
    println!(
        "{} {}Demonizing mtgadmin...!",
        style("[4/4]").bold().dim(),
        TRUCK
    );

    use mtgadmin::daemonize;
    daemonize();

    let deps = 150;
    let pb = ProgressBar::new(deps);
    for _ in 0..deps {
        pb.inc(1);
        thread::sleep(Duration::from_millis(3));
    }
    pb.finish_and_clear();

    /////  END: fn main()  /////
}
