// Import required libraries.
extern crate daemonize;

use daemonize::Daemonize;
use fs::File;
use std::fs;
    
// function() -> Get directory with MTGA game logs.
pub fn get_log_dir(mtg_arena_tool_log_dir: &str) {

    // Import required libraries.
    use std::process::Command;
    
    // Check if MTG Arena Tool log dir exists.
    let mut binding = Command::new("test");
    let logdir = binding.arg("-d").arg(mtg_arena_tool_log_dir);
        
    // Uncomment to debug.
    //println!("Debug info: {:?}", logdir.output());

    // Tell user what MTG Arena Tool log directory we are monitoring.
    println!(
        "Monitoring MTG Arena Tool log directory {}",
        mtg_arena_tool_log_dir
    );

}

/////  BEGIN: fn daemonize()  /////
pub fn daemonize() {

    let stdout = File::create("/tmp/mtgadmin.out").unwrap();
    let stderr = File::create("/tmp/mtgadmin.err").unwrap();

    let daemonize = Daemonize::new()
        .pid_file("/tmp/mtgadmin.pid") // Every method except `new` and `start`
        .chown_pid_file(false) // is optional, see `Daemonize` documentation
        .working_directory("/tmp") // for default behaviour.
        .user("nobody")
        .group("daemon") // Group name
        .group(1) // or group id.
        .umask(0o027) // Set umask, `0o027` by default.
        .stdout(stdout) // Redirect stdout to `/tmp/mtgadmin.out`.
        .stderr(stderr) // Redirect stderr to `/tmp/mtgadmin.err`.
        .privileged_action(|| "Executed before drop privileges");

    match daemonize.start() {
        Ok(_) => println!("Successfully daemonized mtgadmin...!"),
        Err(e) => eprintln!("Unknown error! mtgadmin failed to daemonize...!, {}", e),
    }

    /////  END: fn daemonize()  /////
}

/////  BEGIN: fn get_api_endpoint()  /////
pub fn get_api_endpoint(api_endpoint: &str) {

    //use std::process::Command;

    // Tell user what API endpoint we're about to upload to
    println!("API endpoint to upload game data to: {}", api_endpoint);

    // Did user supply a supported API endpoint?
    // Possible values: 17lands
    // Uncomment below to debug
    //println!("{}", api_endpoint.eq("17lands"));
    
    if api_endpoint.eq("17lands") {
        // Tell user '17lands' is supported.
        println!("17lands is a supported API endpoint!");
    }

    /////  END: fn get_api_endpoint()
}

/////  upload_game_data_to_17lands()  /////
pub fn upload_game_data_to_17lands(mtg_arena_tool_log_dir: &str, api_endpoint: &str) {

    // Import required libraries
    use std::process::Command;
    extern crate execute;
    
    // Tell user we are uploading MTGA game data to '17lands' API endpoint
    println!("Uploading MTGA game data to API endpoint {}", api_endpoint);

    // Upload MTGA game data to API endpoint '17lands'
    for entry in fs::read_dir(mtg_arena_tool_log_dir).unwrap() {
        let entry = entry.unwrap();
        let path = entry.path();

        if path.is_dir() {
            // Found a directory not an MTGA game log file.
            println!("{:?} is a dir", path);
        } else {
            // Found a file. Hopefully it's an MTGA game log file.
            println!("{:?} appears to be an MTGA game log file", path);
            let mut binding = Command::new("seventeenlands");
            let landscmd = binding

                .arg("-l")
                .arg(mtg_arena_tool_log_dir.to_owned() + "{:?}");
        
            // Uncomment to debug.
            println!("Debug info: {:?}", landscmd.output());
            println!("Uploading MTGA game data to 17lands...");
            
        }
    
    }
    ///// END: upload_game_data_to_17lands() /////
}
